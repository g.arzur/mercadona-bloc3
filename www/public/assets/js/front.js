$(document).ready(function(){

    $('.category-link').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href-ajax');
        jQuery.ajax({
            url: url,
            type: 'GET',
            beforeSend: function(){
                jQuery(".product-list").css("opacity", "0.5")
                jQuery(".loader img").show();
            },
            success: function(data){
                jQuery(".product-list").css("opacity", "0.5")
                reloadPageData(data);
            },
            complete : function(){
                jQuery(".loader img").hide();
                jQuery(".product-list").css("opacity", "1")

            }
        });
    });

    function reloadPageData(data){
        reloadProductList(data);
        reloadProductCount(data)
        reloadPageListName(data)
        reloadBreadCrumb(data)
    }

    function reloadProductList(data){
        $('.product-list').html('');
        
        let html = '';
        data.productCollection.forEach(element => {
            html += buildProductHtml(element)
        });

        $('.product-list').html(html);
    }

    function buildProductHtml(product){
        // product.promotion and date(product.promotion.dateDebut) < date() and date(product.promotion.dateFin) > date()
        productDiscounted = (product.promotion && moment(product.promotion.date_debut) < moment() && moment(product.promotion.date_fin) > moment())
        console.log(productDiscounted)

        let productHtml = '';
        productHtml +='<div class="row justify-content-center mb-3">' +
            '<div class="col-md-12">' +
            '<div class="card shadow-0 border rounded-3">' +
            '    <div class="card-body">' +
            '    <div class="row g-0">' +
            '        <div class="col-xl-3 col-md-4 d-flex justify-content-center">' +
            '        <div class="bg-image hover-zoom ripple rounded ripple-surface me-md-3 mb-3 mb-md-0">' +
            '            <img src="'+product.image+'" class="w-100" />' +
            '            <a href="#!">' +
            '            <div class="hover-overlay">' +
            '                <div class="mask" style="background-color: rgba(253, 253, 253, 0.15);"></div>' +
            '            </div>' +
            '            </a>' +
            '        </div>' +
            '        </div>' +
            '        <div class="col-xl-6 col-md-5 col-sm-7">' +
            '        <h5>'+product.libelle+'</h5>' +
            '        <p class="text mb-4 mb-md-0">' +
            '        '+product.description+'' +
            '        </p>' +
            '        </div>' +
            '        <div class="col-xl-3 col-md-3 col-sm-5">' +
            '        <div class="d-flex flex-row align-items-center mb-1">' +
            '            <h4 class="mb-1 me-1 '+ (productDiscounted ? "text-danger" : "") +'">';
            
            if(productDiscounted){
                let price = product.prix - product.promotion.pourcentage_remise / 100 * product.prix;
                productHtml += price.toFixed(2) + '€';
            } else {
                productHtml += product.prix + '€';
            }
            
            productHtml += ' </h4>';


            if(productDiscounted){
                productHtml += 
                '            <span>' +
                '                <s>' +
                '                '+product.prix+'€' +
                '                </s>' +
                '            </span>';
            } 
            productHtml +=
            '        </div>' +
            '        </div>' +
            '    </div>' +
            '    </div>' +
            '</div>' +
            '</div>' +
            '</div>';

            return productHtml;
    }

    function reloadProductCount(data){
        $('.product-collection-count').html(data.productCollection.length+' produits trouvés');
    }
    function reloadPageListName(data){
        $('.page-list-name').html(data.category.libelle);
    }
    function reloadBreadCrumb(data){
        let html = '<a href="/category/'+data.category.id+'" class="current-category-link text-white"><u>'+data.category.libelle+'</u></a>';
        
        if($('.breadcrumb .current-category-link').length == 0){
            $('.breadcrumb h6').append('<span class="text-white-50 mx-2"> > </span>');
            $('.breadcrumb h6').append(html);
        } else {
            $('.breadcrumb .current-category-link').html(html);
        }
    }
});