<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Categorie;
use App\Entity\Produit;
use App\Entity\Promotion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $admin = new Admin();
        $admin->setUsername('admin');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword('$2y$13$YC5lIbYpChP/BfuE1wsLSOksqpb9iuLwpO/VE/1c3wrryjVNCuZHe');
        $manager->persist($admin);


        $blousonsCategory = new Categorie();
        $blousonsCategory->setLibelle('Blousons');
        $manager->persist($blousonsCategory);
        
        $pantalonsCategory = new Categorie();
        $pantalonsCategory->setLibelle('Pantalons');
        $manager->persist($pantalonsCategory);

        $promotion = new Promotion();
        $promotion->setLibelle('-10% sur les blousons');
        $promotion->setPourcentageRemise(10);
        $promotion->setDateDebut(new \DateTime('2023-03-01'));
        $promotion->setDateFin(new \DateTime('2023-12-01'));
        $manager->persist($promotion);

        $product = new Produit();
        $product->setLibelle('Teddy Smith Blouson léger à col Montant - B-Terry');
        $product->setDescription("Col montant Sans manches Poches avant dissimulées dans les coutures Fermeture avec zip métal sur le devant");
        $product->setImage("https://m.media-amazon.com/images/I/81vaImKyu5L.__AC_SX342_SY445_QL70_ML2_.jpg");
        $product->setPrix(50.99);
        $product->setCategorie($blousonsCategory);  
        $product->setPromotion($promotion);  
        $manager->persist($product);

        
        $product = new Produit();
        $product->setLibelle('ERICO HOOD Buffle Cognac');
        $product->setDescription('Blouson col motard avec une capuche Jersey amovible en cuir de buffle résistant qui sera de plus en plus authentique avec la patine du temps. Ce blouson cuir vous donnera le look "d’aventurier/smart" et restera un incontournable de votre dressing de longues années.');
        $product->setImage("https://www.cesarenori.fr/45291-large_default/erico-hood.jpg");
        $product->setPrix(100);
        $product->setCategorie($blousonsCategory);  
        $manager->persist($product);


        $product = new Produit();
        $product->setLibelle('Next Pantalon sans pinces lavable en Machine Coupe Slim Noir Homme');
        $product->setDescription('Taille: Normale
        Fermeture: Braguette avec fermeture éclair
        Poches: Poches arrière, poches latérales
        Motif / Couleur: Couleur unie
        Référence: NX322E0CH-Q11');
        $product->setImage("https://img01.ztat.net/article/spp-media-p1/da6080ace8f33377a44629d6c5b87374/03f200c5661c4bb4ba54b5bb67ebdc35.jpg?imwidth=1800");
        $product->setPrix(28);
        $product->setCategorie($pantalonsCategory);  
        $manager->persist($product);

        $manager->flush();
    }
}
