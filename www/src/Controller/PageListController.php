<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PageListController extends AbstractController
{
    #[Route('/', name: 'app_page_list')]
    public function indexAll(ProduitRepository $produitRepository, CategorieRepository $categorieRepository): Response
    {

        $productCollection = $produitRepository->findAll();
        $categoryCollection = $categorieRepository->findAll();

        return $this->render('page_list/index.html.twig', [
            'controller_name' => 'PageListController',
            'page_list_name' => 'Nos Produits',
            'current_category' => null,
            'productCollection' => $productCollection,
            'categoryCollection' => $categoryCollection,
        ]);
    }

    #[Route('/category/{category}', name: 'app_page_list_filtered')]
    public function indexCategory(Categorie $category, ProduitRepository $produitRepository, CategorieRepository $categorieRepository): Response
    {
        $productCollection = $category->getProduits();
        $categoryCollection = $categorieRepository->findAll();
        
        return $this->render('page_list/index.html.twig', [
            'controller_name' => 'PageListController',
            'page_list_name' => $category->getLibelle(),
            'current_category' => $category,
            'productCollection' => $productCollection,
            'categoryCollection' => $categoryCollection,
        ]);
    }
    #[Route('/ajax/category/{category}', name: 'app_page_list_ajax_filtered')]
    public function ajaxCategory(Categorie $category, ProduitRepository $produitRepository, CategorieRepository $categorieRepository, SerializerInterface $serializer): Response
    {
        $productCollection = $category->getProduits();

        $content = [
            'productCollection' => json_decode($serializer->serialize(
                $productCollection,
                'json', ['groups' => ['promotion', 'categorie', 'produit']]
            )),
            'category' => json_decode($serializer->serialize(
                $category,
                'json', ['groups' => ['categorie', 'produit']]
            )),
        ];

        $response = new JsonResponse($content);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
