<?php

namespace App\Controller\Admin;

use App\Entity\Promotion;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\PercentField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PromotionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Promotion::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Promotion')
            ->setEntityLabelInPlural('Promotions')
            ->setSearchFields(['libelle']);
            // ->setDefaultSort(['createdAt' => 'DESC'])
        ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('libelle'),
            DateTimeField::new('date_debut'),
            DateTimeField::new('date_fin'),
            PercentField::new('pourcentage_remise')
                ->setNumDecimals(2)
                ->setStoredAsFractional(false),
        ];
    }
}
