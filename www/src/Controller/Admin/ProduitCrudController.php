<?php

namespace App\Controller\Admin;

use App\Entity\Produit;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProduitCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Produit::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Produit')
            ->setEntityLabelInPlural('Produits')
            ->setSearchFields(['libelle', 'description']);
            // ->setDefaultSort(['createdAt' => 'DESC'])
        ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('libelle'),
            TextareaField::new('description'),
            TextField::new('image'),
            AssociationField::new('categorie'),
            AssociationField::new('promotion')->setCrudController(PromotionCrudController::class),
            MoneyField::new('prix') 
                ->setNumDecimals(2)
                ->setStoredAsCents(false)
                ->setCurrency('EUR'),
        ];
    }
}
