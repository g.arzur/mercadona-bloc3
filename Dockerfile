FROM php:8.1-apache
RUN docker-php-ext-install mysqli

RUN apt-get update && apt-get install -y \
	nano \
	git \
	libicu-dev \
	zip 
	
RUN docker-php-ext-configure intl
RUN docker-php-ext-install pdo pdo_mysql intl
RUN a2enmod rewrite

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer



##################
# XDEBUG INSTALL #
##################

RUN pecl install xdebug-3.1.5
RUN echo "zend_extension = $(find / -name "xdebug.so" 2>/dev/null)" >> /usr/local/etc/php/conf.d/20-xdebug.ini


RUN echo "[XDebug]" >> /usr/local/etc/php/conf.d/10-custom.ini
RUN echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/10-custom.ini
RUN echo "xdebug.start_with_request=trigger" >> /usr/local/etc/php/conf.d/10-custom.ini
RUN echo "xdebug.client_host=172.51.0.1" >> /usr/local/etc/php/conf.d/10-custom.ini