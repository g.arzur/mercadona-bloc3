# Mercadona

Mercadona is a small e-commerce application. 

## Installation
Run the docker containers
```bash
docker-compose up -d
```

Add the application vhost to /etc/hosts file
```bash
172.56.0.1 caspratique3.local
```

Install dependencies
```bash
# Entering the php container
docker-compose exec php bash

# Installing composer dependencies
composer install

# Applying database schema with doctrine
bin/console doctrine:schema:update --force

# Exit the container
exit
```
Install example data in DB
```bash
# Import default database dump  
docker exec -i caspratique3_mysql mysql -uroot -ptoor caspratique3 < dump.sql

# Exit the container
exit
```

## Usage

### Start Project / Stop Project
```bash
# Start project
docker-compose up -d

# Stop project
docker-compose stop
```

### Access FrontOffice
Url :  http://caspratique3.local/

### Access Backoffice
Url : http://caspratique3.local/login

Backoffice credentials : admin / admin 


## License

[MIT](https://choosealicense.com/licenses/mit/)